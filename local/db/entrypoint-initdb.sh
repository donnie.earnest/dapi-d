#!/bin/bash

echo "#########################  CREATING ${DB_NAME} DATABASE  #########################"
if ! mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "USE ${DB_NAME}"; then
  mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';"
  mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE ${DB_NAME} CHARACTER SET utf8 COLLATE utf8_general_ci;"
  mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "CREATE USER ${DB_USER}@'%' IDENTIFIED BY '${DB_PASS}';"
  mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USER}'@'%';"
  mysql -uroot -p${MYSQL_ROOT_PASSWORD} -e "FLUSH PRIVILEGES;"
fi
echo
