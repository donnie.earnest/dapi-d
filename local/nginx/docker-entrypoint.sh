#!/bin/bash

echo "#########################  WAITING FOR DJANGO  #########################"
until $(curl --output /dev/null --silent --head --fail http://app:${DJANGOPORT}); do
    sleep 5
done
echo

echo "#########################  STARTING WEB SERVER  #########################"
/usr/sbin/nginx -g "daemon off;"
