
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = "main"
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='main'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
