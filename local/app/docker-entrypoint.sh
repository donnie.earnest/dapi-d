#!/bin/bash

echo "#########################  WAITING FOR DATABASE  #########################"
# TODO: need better way to wait for database connection here
sleep 10

# if manage.py doesn't exist, this must be the first time it's being executed
file=/var/www/manage.py
if [ ! -f "$file" ]; then

    echo "#########################  CREATING DJANGO PROJECT  #########################"

    # create django project
    django-admin startproject main .

    # modify django settings
    settings='main/settings.py'
    sed -i "s/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS = \['*'\]/" $settings
    sed -i "/^INSTALLED_APPS/a \ \ \ \ 'main'," $settings
    sed -i "/# Database/,+10d" $settings
    cat /usr/src/settings.py >> $settings
    yes | cp -rf /usr/src/main /var/www

    # create static folder & collect content
    echo
    mkdir -p /var/www/static/css
    mv /usr/src/style.css /var/www/static/css
    python manage.py collectstatic --noinput

    # migrate django models first time
    python manage.py makemigrations --noinput
    python manage.py migrate --noinput
   
    # create super user
    echo
    python manage.py createsuperuser --noinput

    # create requirements.txt
    echo
    python -m pip freeze > requirements.txt

    echo
fi

echo "#########################  MIGRATING DJANGO MODELS  #########################"
python manage.py makemigrations --noinput
python manage.py migrate --noinput
echo

echo "#########################  STARTING DJANGO  #########################"
python --version
/usr/local/bin/gunicorn main.wsgi:application -w 2 -b :8000 --reload
